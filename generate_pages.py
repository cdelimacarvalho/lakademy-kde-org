import os

import toml

EDITIONS_PATH = "./editions"
LANGS = ("pt", "es", "en")


def get_editions_parameters():
    editions_params = []
    for edition_file in os.listdir(EDITIONS_PATH):
        full_path = os.path.join(EDITIONS_PATH, edition_file)
        with open(full_path, "r") as params:
            edition_params = toml.loads(params.read())
            editions_params.append(edition_params)

    editions_params = sorted(
        editions_params, key=lambda edition: edition["edition_year"], reverse=True
    )

    return editions_params


def get_editions_section(editions_params):

    editions_href_list = []
    for edition_params in editions_params:
        edition_html_path = f"{edition_params['edition_code']}.html"
        edition_year = edition_params["edition_year"]
        edition_href = f'<li><a href="{edition_html_path}">{edition_year}</a></li>'
        editions_href_list.append(edition_href)

    editions_section = '<ul class="hidden">' + "\n".join(editions_href_list) + "</ul>"

    return editions_section


def load_template(template_name):
    with open(f"./templates/{template_name}_template.html", "r") as f:
        template_content = f.read()
    return template_content


def load_lang_texts():
    with open("./templates/lang_texts.toml", "r") as f:
        lang_texts = toml.loads(f.read())
    return lang_texts


def fill_edition_template_content(content, edition_params, editions_section):
    edition_code = edition_params["edition_code"]
    edition_content = content[:]
    edition_content = edition_content.replace("{% editions %}", editions_section)
    edition_content = edition_content.replace("{% edition_code %}", edition_code)
    for key, content in edition_params.items():
        if key != "edition_info":
            edition_content = edition_content.replace(f"{{% {key} %}}", content)
    return edition_content


def fill_sections(page_content, lang_texts):
    page_contents = {}
    for lang in LANGS:
        lang_page_content = page_content[:]

        for section in (
            "home",
            "about",
            "editions_text",
            "contact_page",
            "donate",
            "footer",
        ):
            lang_page_content = lang_page_content.replace(
                f"{{% {section} %}}", lang_texts[lang][section]
            )
        page_contents[lang] = lang_page_content

    return page_contents


def fill_edition_info(page_contents, edition_params):
    for lang in LANGS:
        edition_info_key = "edition_info_" + lang
        lang_page_content = page_contents[lang]
        lang_page_content = lang_page_content.replace(
            "{% edition_info %}", edition_params[edition_info_key]
        )
        page_contents[lang] = lang_page_content
    return page_contents


def save_contents(page_name, page_contents_by_lang):
    for lang in LANGS:
        lang_page_content = page_contents_by_lang[lang]
        result_html_path = page_name + ("-" + lang if lang != "pt" else "") + ".html"
        with open(result_html_path, "w") as result_file:
            result_file.write(lang_page_content)


def fill_index_slides(index_content_by_lang, editions_params):

    slide_template = """<li><img src="{% group_photo %}" alt="" title="" border="0"/><div class="flex-caption"><h2>{% edition_name %}</h2>    <p>{% edition_summary %}</p><a href="{% edition_path %}" class="slider_button">Read more</a></div></li>"""

    for lang in LANGS:
        slides = []
        index_content = index_content_by_lang[lang]

        for edition_params in editions_params:
            edition_summary_key = "edition_summary_" + lang
            edition_summary = edition_params[edition_summary_key]
            slide_template_lang = slide_template[:]
            slide_template_lang = slide_template_lang.replace(
                "{% group_photo %}", edition_params["edition_group_photo"]
            )
            slide_template_lang = slide_template_lang.replace(
                "{% edition_name %}", edition_params["edition_name"]
            )
            slide_template_lang = slide_template_lang.replace(
                "{% edition_summary %}", edition_summary
            )
            edition_path = edition_params["edition_code"] + (
                "-" + lang if lang != "pt" else ""
            )
            slide_template_lang = slide_template_lang.replace(
                "{% edition_path %}", edition_path
            )
            slides.append(slide_template_lang)

        index_content = index_content.replace(
            "{% editions_slides %}", "\n".join(slides)
        )
        index_content_by_lang[lang] = index_content
    return index_content_by_lang


def fill_text(page_name, content_by_lang, lang_texts):
    for lang in LANGS:
        content = content_by_lang[lang]
        lang_text = lang_texts[lang][f"{page_name}_text"]
        content = content.replace(f"{{% {page_name}_text %}}", lang_text)
        content_by_lang[lang] = content
    return content_by_lang


def fill_page(page_name, editions_params, editions_section, lang_texts):
    content = load_template(page_name)
    content = content.replace("{% editions %}", editions_section)
    content_by_lang = fill_sections(content, lang_texts)
    content_by_lang = fill_text(page_name, content_by_lang, lang_texts)
    if page_name == "index":
        content_by_lang = fill_index_slides(content_by_lang, editions_params)
    return content_by_lang


def main():

    editions_params = get_editions_parameters()
    editions_section = get_editions_section(editions_params)

    content = load_template("edition")
    lang_texts = load_lang_texts()

    for edition_params in editions_params:
        edition_content = fill_edition_template_content(
            content, edition_params, editions_section
        )
        edition_content_by_lang = fill_sections(edition_content, lang_texts)
        edition_content_by_lang = fill_edition_info(
            edition_content_by_lang, edition_params
        )
        save_contents(edition_params["edition_code"], edition_content_by_lang)

    for page_name in ("index", "about", "contact"):
        contents_by_lang = fill_page(
            page_name, editions_params, editions_section, lang_texts
        )
        save_contents(page_name, contents_by_lang)


if __name__ == "__main__":
    main()
