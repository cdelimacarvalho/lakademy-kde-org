run-isort:
	~/Library/Python/3.8/bin/isort $(ISORT_ARGS) .

run-black:
	black $(BLACK_ARGS) .

.PHONY: lint
lint: ISORT_ARGS = "--check"
lint: BLACK_ARGS = "--check"
lint: run-isort run-black
	flake8 .

.PHONY: format
format: run-isort run-black

.PHONY: setup
setup:
	pip3 install -r requirements.txt

.PHONY: run
run:
	python3 generate_pages.py

.PHONY: clean
clean:
	rm *.html
