# LaKademy website

This website uses a simple HTML render script to generate pages for each edition of the event.

## Creating a new edition page

To create a new edition, you will need to include its respective `.toml` inside of the `editions` folder.

Example:

```
edition_code = 'lakademy20'
edition_name = 'LaKademy 2020'
edition_year = '2020'
edition_logo_path = 'images/ArteLakademy2020.png'
edition_group_photo = 'images/GroupPhoto2020.png'
edition_location = '' # Location coordenates
edition_summary_pt = '' # Edition summary in Portuguese
edition_summary_en = '' # Edition summary in English
edition_summary_es = '' # Edition summary in Spanish
edition_info_pt = '' # Edition information in Portuguese (can be HTML or plain text)
edition_info_en = '' # Edition information in English
edition_info_es = '' # Edition information in Spanish
```

## Setup

```
make setup
```

## Running

```
make run
```
